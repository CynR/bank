public class MonetaryAmount {
    double amount;
    private String currency;

    protected MonetaryAmount(double amount, String currency){
        this.amount = amount;
        this.currency = currency;
    }

    void addAmount(double a){
        amount = amount + a;
    }

    void substractAmount(double a){
        amount = amount - a;
    }

    public String toString(){
        String text = "The current balance is ";
        return text + amount + " " + currency;
    }

    public double getAmount(){
        return amount;
    }

}
