import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestAccountCourrant {
    @Test
    void showTheCurrentBalanceAfterStartDepositWithdrawalAccountCourrant(){
        String name = "Cynthia";
        String cur = "dollars";
        Account cynAccount = new AccountCourant(name,cur);

        //Inicial Balance
        Assertions.assertEquals("The current balance is " + "0.0" + " " + "dollars", cynAccount.getCurrentBalance().toString());
        //First Deposit
        cynAccount.deposit(150);
        Assertions.assertEquals("The current balance is " + "150.0" + " " + "dollars",cynAccount.getCurrentBalance().toString());
        //First Withdrawal
        cynAccount.withdraw(50);
        Assertions.assertEquals("The current balance is " + "100.0" + " " + "dollars",cynAccount.getCurrentBalance().toString());
        //Second Withdrawal
        cynAccount.withdraw(1000);
        Assertions.assertEquals("The current balance is " + "-900.0" + " " + "dollars",cynAccount.getCurrentBalance().toString());
        //Second Withdrawal = not possible to take out money because is more then the available in th account
        // -> the current balance is unaffected
        cynAccount.withdraw(101);
        Assertions.assertEquals("The current balance is " + "-900.0" + " " + "dollars",cynAccount.getCurrentBalance().toString());
        //Asking who is the owner
        Assertions.assertEquals("Cynthia", cynAccount.getOwnerName());
    }


}
