public class AccountEpargne extends Account{
    double interest;
    double amount;
    public AccountEpargne(String ownerName, String currency, double amoC, double interest){
        super(ownerName,currency);
        this.amount = amoC;
        this.interest = interest;
        this.balance = new MonetaryAmount(amoC,currency);

    }

    void deposit(double amount){
        balance.addAmount(amount);
    }

    void withdraw(double amount) {
        if (balance.getAmount() - amount >= 0) {
            balance.substractAmount(amount);
        }
    }

    public MonetaryAmount getCurrentBalance(){
        return balance;
    }

    public String getOwnerName(){
        return ownerName;
    }

    public void crediterInterets(MonetaryAmount b){
        b.amount = b.getAmount()*(1+interest);
    }

}
