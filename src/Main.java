

public class Main {
    public static void main(String[] args) {
        Account accountObeSes = new Account("Obelix", "sesterces");
        afficher(accountObeSes);
        accountObeSes.deposit((500.5));
        afficher(accountObeSes);
        accountObeSes.withdraw(125.25);
        afficher(accountObeSes);
        accountObeSes.withdraw(1337.0);
        afficher(accountObeSes);

        System.out.println("Partie 2 :)");
        Account accoCourObeSes = new AccountCourant("Obelix", "sesterces");
        AccountEpargne accoEpaObeDoll = new AccountEpargne("Obelix", "dollars", 1, 0.01);
        accoCourObeSes.deposit(100);
        accoEpaObeDoll.deposit(30);
        accoCourObeSes.withdraw(200);
        System.out.println(accoCourObeSes.getCurrentBalance().toString());
        accoEpaObeDoll.crediterInterets(accoEpaObeDoll.getCurrentBalance());
        System.out.println(accoEpaObeDoll.getCurrentBalance().toString());

    }

    private static void afficher(Account accountObeSes) {
        System.out.println(accountObeSes.getCurrentBalance().toString());
    }
}
