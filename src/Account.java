public class Account {
    String ownerName;
    MonetaryAmount balance;
    protected Account(String ownerName, String currency){
        this.ownerName = ownerName;
        this.balance = new MonetaryAmount(0,currency);

    }

    void deposit(double amount){
        balance.addAmount(amount);
    }

    void withdraw(double amount) {
        if (balance.getAmount() - amount >= 0) {
            balance.substractAmount(amount);
        }
    }

    public MonetaryAmount getCurrentBalance(){
        return balance;
    }

    public String getOwnerName(){
        return ownerName;
    }

}
