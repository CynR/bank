public class AccountCourant extends Account{
    String ownerName;
    MonetaryAmount balance;
    protected AccountCourant(String ownerName, String currency){
        super(ownerName,currency);
        this.ownerName = ownerName;
        this.balance = new MonetaryAmount(0,currency);

    }

    void deposit(double amount){
        balance.addAmount(amount);
    }

    void withdraw(double amount) {
        if (balance.getAmount() - amount >= -1000) {
            balance.substractAmount(amount);
        }
    }

    public MonetaryAmount getCurrentBalance(){
        return balance;
    }

    public String getOwnerName(){
        return ownerName;
    }

}
