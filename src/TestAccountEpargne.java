import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestAccountEpargne {
    @Test
    void showTheCurrentBalanceAfterStartDepositWithdrawalAccountEpargne(){
        String name = "Cynthia";
        String cur = "pesos";
        double money = 200;
        double inte = 0.05;
        AccountEpargne cynAccount = new AccountEpargne(name,cur,money,inte);

        //Inicial Balance
        Assertions.assertEquals("The current balance is " + "200.0" + " " + "pesos", cynAccount.getCurrentBalance().toString());
        //First Deposit
        cynAccount.deposit(150);
        Assertions.assertEquals("The current balance is " + "350.0" + " " + "pesos",cynAccount.getCurrentBalance().toString());
        //First Withdrawal
        cynAccount.withdraw(50);
        Assertions.assertEquals("The current balance is " + "300.0" + " " + "pesos",cynAccount.getCurrentBalance().toString());
        //Second Withdrawal = not possible to take out money because is more then the available in th account
        // -> the current balance is unaffected
        cynAccount.withdraw(301);
        Assertions.assertEquals("The current balance is " + "300.0" + " " + "pesos",cynAccount.getCurrentBalance().toString());
        //Get Balance with Yield Value
        cynAccount.crediterInterets(cynAccount.getCurrentBalance());
        Assertions.assertEquals("The current balance is " + "315.0" + " " + "pesos",cynAccount.getCurrentBalance().toString());
        //Asking who is the owner
        Assertions.assertEquals("Cynthia", cynAccount.getOwnerName());
    }

}
